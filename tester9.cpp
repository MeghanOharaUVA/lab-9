#include <iostream>

using namespace std; 

class Animal {
  int weight, height;
public:
  Animal()
  {height = 800;
    weight = 500; }
  virtual int getHeight() 
  { return this->height;}

  virtual int getWeight()
  {return this->weight;}
};

class Giraffe: public Animal {
  int heightWithoutNeck, giraffeWeight;
public:
  Giraffe()
  {heightWithoutNeck = 9000;
    giraffeWeight = 700;}
  virtual int getHeight()
  { return this->heightWithoutNeck; }

  virtual int getWeight()
  {return this->giraffeWeight;}
};


int main () {
  int which;
  Animal *a; 
  cin >> which;
  if ( which == 0 ) {            
    a = new Animal(); 
  }
  else {
    a = new Giraffe();
  }
  int heightRet = a->getHeight();
  int weightRet = a->getWeight(); 
  return 0; 
}
