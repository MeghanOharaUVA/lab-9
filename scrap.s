LBB0_7:
	mov	rax, qword ptr [rbp - 16]
	mov	rcx, qword ptr [rax]
	mov	rdi, rax
	call	qword ptr [rcx]
	mov	dword ptr [rbp - 32], eax
	mov	rcx, qword ptr [rbp - 16]
	mov	rdi, qword ptr [rcx]
	mov	qword ptr [rbp - 88], rdi ## 8-byte Spill
	mov	rdi, rcx
	mov	rcx, qword ptr [rbp - 88] ## 8-byte Reload
	call	qword ptr [rcx + 8]
	xor	edx, edx
	mov	dword ptr [rbp - 36], eax
	mov	eax, edx
	add	rsp, 96
	pop	rbp
	ret
