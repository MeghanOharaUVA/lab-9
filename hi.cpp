#include "timer.h"
#include <iostream>
#include <cstdlib>



int recurseFunc(int x) {

  if (x != 1) {
    int remainder = x % 2;
    if (remainder == 0) {
      x = x / 2;
      return recurseFunc(x) + 1;
    }

    else {
      x = 3 * x + 1;
      return recurseFunc(x) + 1;
    }
  }
  else {
    return 0;
  }
}
  
int main() {
  timer t; 
  int x;
  int result;
  cout << "Enter input x: " << endl;
  cin >> x;

  int n; 
  cout << "Enter input n: " << endl;
  cin >> n;

  t.start(); 
  for(int i = 0; i < n; i++) {
    result = recurseFunc(x);
  }
  t.stop();
  double totTime = t.getTime();
  double avgTime = totTime/n;

  cout << "Steps to 1: " << result << endl;
  cout << "Average time: " << avgTime << endl;

}

  
  
