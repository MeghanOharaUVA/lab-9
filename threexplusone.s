	; Meghan OHara mmo2bf 11/14/16 threexplusone.s

	global _Func_recurse

	section .text

	;;  threexplusone.s
	;; x - user input reduced to 1


_Func_recurse:
	;; standard prologue
	mov 	rbp, rsp	; set new value of base pointer

	mov	rdx, [rbp+8] 	; x
	
	cmp	rdx, 1		; if x == 1
	je	Func_one	; end
	Test	dl, 1		; bitwise AND - test last bit
	jnz	Func_odd	; if odd, go to odd if

Func_even:
	shr	rdx, 1		; x = x / 2 - OPTIMIZATION
				; bit shift instead of division

	push	rdx		; push param
	
	call	_Func_recurse	; recursion
	inc	rax		; count++

	add	rsp, 4		; pop param

Func_odd:
	lea	rdx, [2*rdx + rdx + 1] ; x = 3x +1 OPTIMIZATION
				; used lea to do all in one line
	push	rdx		; push param
	
	call	_Func_recurse	; recursion
	inc	rax		; count++

	add	rsp, 4		; pop param

Func_one:
	xor	rax, rax	; set eax to zero

Func_done:	 
	;;  standard epilogue
                 		; restore the callers base pionter
	ret			; return to the caller 
