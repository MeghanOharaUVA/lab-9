#include <stdlib.h>
#include <stdio.h>

struct listNode {
  struct listNode *nextNode, *prevNode;
  int *val; 
};
struct list {
  struct listNode *head; 
}; 
    

int main() {
  /* read in int n*/
  int n;
  scanf("N: %d", &n);
  struct listNode *curr;
  struct list *myList = (struct list *) malloc (sizeof(struct list));
  curr = myList->head; 
  
  // get n more ints 
  for (int i = 0; i < n; i++) {
    struct listNode *temp; 
    temp = (struct listNode *) malloc(sizeof(struct listNode)); 
    int nodeVal;

    scanf("Node value: %d", &nodeVal);
    temp->val = &nodeVal;

    curr->nextNode = temp;
    temp->prevNode  = curr;
    curr = temp; 
  }
  
  // print out the list
  for (int i = 0; i < n; i++) {
    printf("%s", curr->val);
    curr = curr->prevNode;
  }
  
  // deallocate the list 
  free(myList); 
}



