	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 12
	.intel_syntax noprefix
	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI0_0:
	.long	500                     ## 0x1f4
	.long	800                     ## 0x320
	.long	9000                    ## 0x2328
	.long	700                     ## 0x2bc
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp2:
	.cfi_def_cfa_register rbp
	push	rbx
	push	rax
Ltmp3:
	.cfi_offset rbx, -24
	mov	rdi, qword ptr [rip + __ZNSt3__13cinE@GOTPCREL]
	lea	rsi, [rbp - 12]
	call	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEErsERi
	cmp	dword ptr [rbp - 12], 0
	je	LBB0_1
## BB#2:
	mov	edi, 24
	call	__Znwm
	mov	rbx, rax
	mov	rax, qword ptr [rip + __ZTV7Giraffe@GOTPCREL]
	add	rax, 16
	mov	qword ptr [rbx], rax
	movaps	xmm0, xmmword ptr [rip + LCPI0_0] ## xmm0 = [500,800,9000,700]
	movups	xmmword ptr [rbx + 8], xmm0
	jmp	LBB0_3
LBB0_1:
	mov	edi, 16
	call	__Znwm
	mov	rbx, rax
	mov	rax, qword ptr [rip + __ZTV6Animal@GOTPCREL]
	add	rax, 16
	mov	qword ptr [rbx], rax
	movabs	rax, 3435973837300
	mov	qword ptr [rbx + 8], rax
LBB0_3:
	mov	rax, qword ptr [rbx]
	mov	rdi, rbx
	call	qword ptr [rax]
	mov	rax, qword ptr [rbx]
	mov	rdi, rbx
	call	qword ptr [rax + 8]
	xor	eax, eax
	add	rsp, 8
	pop	rbx
	pop	rbp
	ret
	.cfi_endproc
			
	.globl	__ZN6Animal9getHeightEv
	.weak_def_can_be_hidden	__ZN6Animal9getHeightEv
	.align	4, 0x90
__ZN6Animal9getHeightEv:                ## @_ZN6Animal9getHeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp4:
	.cfi_def_cfa_offset 16
Ltmp5:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp6:
	.cfi_def_cfa_register rbp
	mov	eax, dword ptr [rdi + 12]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN6Animal9getWeightEv
	.weak_def_can_be_hidden	__ZN6Animal9getWeightEv
	.align	4, 0x90
__ZN6Animal9getWeightEv:                ## @_ZN6Animal9getWeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp7:
	.cfi_def_cfa_offset 16
Ltmp8:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp9:
	.cfi_def_cfa_register rbp
	mov	eax, dword ptr [rdi + 8]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7Giraffe9getHeightEv
	.weak_def_can_be_hidden	__ZN7Giraffe9getHeightEv
	.align	4, 0x90
__ZN7Giraffe9getHeightEv:               ## @_ZN7Giraffe9getHeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp10:
	.cfi_def_cfa_offset 16
Ltmp11:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp12:
	.cfi_def_cfa_register rbp
	mov	eax, dword ptr [rdi + 16]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7Giraffe9getWeightEv
	.weak_def_can_be_hidden	__ZN7Giraffe9getWeightEv
	.align	4, 0x90
__ZN7Giraffe9getWeightEv:               ## @_ZN7Giraffe9getWeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp13:
	.cfi_def_cfa_offset 16
Ltmp14:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp15:
	.cfi_def_cfa_register rbp
	mov	eax, dword ptr [rdi + 20]
	pop	rbp
	ret
	.cfi_endproc

	.section	__DATA,__data
	.globl	__ZTV6Animal            ## @_ZTV6Animal
	.weak_def_can_be_hidden	__ZTV6Animal
	.align	3
__ZTV6Animal:
	.quad	0
	.quad	__ZTI6Animal
	.quad	__ZN6Animal9getHeightEv
	.quad	__ZN6Animal9getWeightEv

	.section	__TEXT,__const
	.globl	__ZTS6Animal            ## @_ZTS6Animal
	.weak_definition	__ZTS6Animal
__ZTS6Animal:
	.asciz	"6Animal"

	.section	__DATA,__data
	.globl	__ZTI6Animal            ## @_ZTI6Animal
	.weak_definition	__ZTI6Animal
	.align	3
__ZTI6Animal:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS6Animal

	.globl	__ZTV7Giraffe           ## @_ZTV7Giraffe
	.weak_def_can_be_hidden	__ZTV7Giraffe
	.align	3
__ZTV7Giraffe:
	.quad	0
	.quad	__ZTI7Giraffe
	.quad	__ZN7Giraffe9getHeightEv
	.quad	__ZN7Giraffe9getWeightEv

	.section	__TEXT,__const
	.globl	__ZTS7Giraffe           ## @_ZTS7Giraffe
	.weak_definition	__ZTS7Giraffe
__ZTS7Giraffe:
	.asciz	"7Giraffe"

	.section	__DATA,__data
	.globl	__ZTI7Giraffe           ## @_ZTI7Giraffe
	.weak_definition	__ZTI7Giraffe
	.align	4
__ZTI7Giraffe:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS7Giraffe
	.quad	__ZTI6Animal


.subsections_via_symbols
