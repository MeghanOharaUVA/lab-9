// Meghan O'Hara mmo2bf 11/14/16 threexinput.cpp
extern "C" long Func_recurse(long); 

#include "timer.h"
#include <iostream>
#include <cstdlib>

using namespace std;


int main() {
  timer t; 
  long x;
  long result;
  cout << "Enter input x: " << endl;
  cin >> x;

  int n; 
  cout << "Enter input n: " << endl;
  cin >> n;

  t.start(); 
  for(int i = 0; i < n; i++) {
    result = Func_recurse(x);
  }
  t.stop();
  double totTime = t.getTime();
  double avgTime = totTime/n * 1000;

  cout << "Steps to 1: " << result << endl;
  cout << "Average time: " << avgTime << endl;
  return 0;
}
