	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 12
	.intel_syntax noprefix
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	push	rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp8:
	.cfi_def_cfa_register rbp
	sub	rsp, 96
	mov	rdi, qword ptr [rip + __ZNSt3__13cinE@GOTPCREL]
	lea	rsi, [rbp - 8]
	mov	dword ptr [rbp - 4], 0
	call	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEErsERi
	cmp	dword ptr [rbp - 8], 0
	mov	qword ptr [rbp - 48], rax ## 8-byte Spill
	jne	LBB0_4
## BB#1:
	mov	eax, 16
	mov	edi, eax
	call	__Znwm
	mov	rdi, rax
	mov	rcx, rax
Ltmp3:
	mov	qword ptr [rbp - 56], rdi ## 8-byte Spill
	mov	rdi, rax
	mov	qword ptr [rbp - 64], rcx ## 8-byte Spill
	call	__ZN6AnimalC1Ev
Ltmp4:
	jmp	LBB0_2
LBB0_2:
	mov	rax, qword ptr [rbp - 64] ## 8-byte Reload
	mov	qword ptr [rbp - 16], rax
	jmp	LBB0_7
LBB0_3:
Ltmp5:
	mov	ecx, edx
	mov	qword ptr [rbp - 24], rax
	mov	dword ptr [rbp - 28], ecx
	mov	rdi, qword ptr [rbp - 56] ## 8-byte Reload
	call	__ZdlPv
	jmp	LBB0_8
LBB0_4:
	mov	eax, 24
	mov	edi, eax
	call	__Znwm
	mov	rdi, rax
	mov	rcx, rax
Ltmp0:
	mov	qword ptr [rbp - 72], rdi ## 8-byte Spill
	mov	rdi, rax
	mov	qword ptr [rbp - 80], rcx ## 8-byte Spill
	call	__ZN7GiraffeC1Ev
Ltmp1:
	jmp	LBB0_5
LBB0_5:
	mov	rax, qword ptr [rbp - 80] ## 8-byte Reload
	mov	qword ptr [rbp - 16], rax
	jmp	LBB0_7
LBB0_6:
Ltmp2:
	mov	ecx, edx
	mov	qword ptr [rbp - 24], rax
	mov	dword ptr [rbp - 28], ecx
	mov	rdi, qword ptr [rbp - 72] ## 8-byte Reload
	call	__ZdlPv
	jmp	LBB0_8
LBB0_7:
	mov	rax, qword ptr [rbp - 16]
	mov	rcx, qword ptr [rax]
	mov	rdi, rax
	call	qword ptr [rcx]
	mov	dword ptr [rbp - 32], eax
	mov	rcx, qword ptr [rbp - 16]
	mov	rdi, qword ptr [rcx]
	mov	qword ptr [rbp - 88], rdi ## 8-byte Spill
	mov	rdi, rcx
	mov	rcx, qword ptr [rbp - 88] ## 8-byte Reload
	call	qword ptr [rcx + 8]
	xor	edx, edx
	mov	dword ptr [rbp - 36], eax
	mov	eax, edx
	add	rsp, 96
	pop	rbp
	ret
LBB0_8:
	mov	rdi, qword ptr [rbp - 24]
	call	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp3-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp3
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp3-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset3
Lset4 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp4-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp0-Ltmp4                     ##   Call between Ltmp4 and Ltmp0
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset7 = Ltmp0-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset7
Lset8 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset8
Lset9 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp1-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset10
Lset11 = Lfunc_end0-Ltmp1               ##   Call between Ltmp1 and Lfunc_end0
	.long	Lset11
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZN6AnimalC1Ev
	.weak_def_can_be_hidden	__ZN6AnimalC1Ev
	.align	4, 0x90
__ZN6AnimalC1Ev:                        ## @_ZN6AnimalC1Ev
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp11:
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	call	__ZN6AnimalC2Ev
	add	rsp, 16
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7GiraffeC1Ev
	.weak_def_can_be_hidden	__ZN7GiraffeC1Ev
	.align	4, 0x90
__ZN7GiraffeC1Ev:                       ## @_ZN7GiraffeC1Ev
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp14:
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	call	__ZN7GiraffeC2Ev
	add	rsp, 16
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN6AnimalC2Ev
	.weak_def_can_be_hidden	__ZN6AnimalC2Ev
	.align	4, 0x90
__ZN6AnimalC2Ev:                        ## @_ZN6AnimalC2Ev
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp17:
	.cfi_def_cfa_register rbp
	mov	rax, qword ptr [rip + __ZTV6Animal@GOTPCREL]
	add	rax, 16
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	qword ptr [rdi], rax
	mov	dword ptr [rdi + 12], 800
	mov	dword ptr [rdi + 8], 500
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN6Animal9getHeightEv
	.weak_def_can_be_hidden	__ZN6Animal9getHeightEv
	.align	4, 0x90
__ZN6Animal9getHeightEv:                ## @_ZN6Animal9getHeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp20:
	.cfi_def_cfa_register rbp
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	eax, dword ptr [rdi + 12]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN6Animal9getWeightEv
	.weak_def_can_be_hidden	__ZN6Animal9getWeightEv
	.align	4, 0x90
__ZN6Animal9getWeightEv:                ## @_ZN6Animal9getWeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp23:
	.cfi_def_cfa_register rbp
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	eax, dword ptr [rdi + 8]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7GiraffeC2Ev
	.weak_def_can_be_hidden	__ZN7GiraffeC2Ev
	.align	4, 0x90
__ZN7GiraffeC2Ev:                       ## @_ZN7GiraffeC2Ev
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp26:
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	rax, rdi
	mov	qword ptr [rbp - 16], rdi ## 8-byte Spill
	mov	rdi, rax
	call	__ZN6AnimalC2Ev
	mov	rax, qword ptr [rip + __ZTV7Giraffe@GOTPCREL]
	add	rax, 16
	mov	rdi, qword ptr [rbp - 16] ## 8-byte Reload
	mov	qword ptr [rdi], rax
	mov	dword ptr [rdi + 16], 9000
	mov	dword ptr [rdi + 20], 700
	add	rsp, 16
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7Giraffe9getHeightEv
	.weak_def_can_be_hidden	__ZN7Giraffe9getHeightEv
	.align	4, 0x90
__ZN7Giraffe9getHeightEv:               ## @_ZN7Giraffe9getHeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp27:
	.cfi_def_cfa_offset 16
Ltmp28:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp29:
	.cfi_def_cfa_register rbp
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	eax, dword ptr [rdi + 16]
	pop	rbp
	ret
	.cfi_endproc

	.globl	__ZN7Giraffe9getWeightEv
	.weak_def_can_be_hidden	__ZN7Giraffe9getWeightEv
	.align	4, 0x90
__ZN7Giraffe9getWeightEv:               ## @_ZN7Giraffe9getWeightEv
	.cfi_startproc
## BB#0:
	push	rbp
Ltmp30:
	.cfi_def_cfa_offset 16
Ltmp31:
	.cfi_offset rbp, -16
	mov	rbp, rsp
Ltmp32:
	.cfi_def_cfa_register rbp
	mov	qword ptr [rbp - 8], rdi
	mov	rdi, qword ptr [rbp - 8]
	mov	eax, dword ptr [rdi + 20]
	pop	rbp
	ret
	.cfi_endproc

	.section	__DATA,__data
	.globl	__ZTV6Animal            ## @_ZTV6Animal
	.weak_def_can_be_hidden	__ZTV6Animal
	.align	3
__ZTV6Animal:
	.quad	0
	.quad	__ZTI6Animal
	.quad	__ZN6Animal9getHeightEv
	.quad	__ZN6Animal9getWeightEv

	.section	__TEXT,__const
	.globl	__ZTS6Animal            ## @_ZTS6Animal
	.weak_definition	__ZTS6Animal
__ZTS6Animal:
	.asciz	"6Animal"

	.section	__DATA,__data
	.globl	__ZTI6Animal            ## @_ZTI6Animal
	.weak_definition	__ZTI6Animal
	.align	3
__ZTI6Animal:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS6Animal

	.globl	__ZTV7Giraffe           ## @_ZTV7Giraffe
	.weak_def_can_be_hidden	__ZTV7Giraffe
	.align	3
__ZTV7Giraffe:
	.quad	0
	.quad	__ZTI7Giraffe
	.quad	__ZN7Giraffe9getHeightEv
	.quad	__ZN7Giraffe9getWeightEv

	.section	__TEXT,__const
	.globl	__ZTS7Giraffe           ## @_ZTS7Giraffe
	.weak_definition	__ZTS7Giraffe
__ZTS7Giraffe:
	.asciz	"7Giraffe"

	.section	__DATA,__data
	.globl	__ZTI7Giraffe           ## @_ZTI7Giraffe
	.weak_definition	__ZTI7Giraffe
	.align	4
__ZTI7Giraffe:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS7Giraffe
	.quad	__ZTI6Animal


.subsections_via_symbols
